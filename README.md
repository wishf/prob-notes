prob-notes
----------

My probability notes - cleaned up and LaTeX-ified as part of my revision.

Each section gets it's own folder, structured as follows:

* _sets_ - Set Theory Primer (covering the stuff needed for the course)
* _discrete_ - Discrete probability primer, distributions and examples
* _continuous_ - Continuous probability primer, distributions and examples
* _joint_ - Joint probability distributions with examples
* _conditional_ - Conditional probability distributions with examples
* _moment_ - Moment generating functions with examples
* _markov_ - Markov chains (discrete and continuous) with examples (including queueing theory)
* _entropy_ - Information theoretic content covered in the course (not all covered at time of writing)

The idea is that it might be nice to stitch these together into a mini-book of sorts.

There is also one additional section:

* _cheatsheet_ - A listing of important formulas and theorems (hopefully some nice text descriptions if I can make it fit)

Dependencies
------------

Currently, this document depends on the following LaTeX packages:

* mathtools
* multirow
* booktabs

Building
--------

Use the provided ```build.sh``` file to construct PDFs of either individual sections or the entire mini-book.

Usage information is below, but can also be retrieved by calling build.sh with no arguments:

```
./build.sh [-v] [book|section <section name>]
```